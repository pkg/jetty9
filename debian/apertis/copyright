Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: rights in its Contribution, if any, to grant the copyright
License: Apache-2.0 and/or EPL-1.0

Files: LICENSE-CONTRIBUTOR/*
Copyright: the combination of files containing Original Software with files containing Modifications, in each case including portions thereof.
License: CDDL-1.0

Files: apache-jsp/src/test/resources/* examples/embedded/src/main/resources/* jetty-alpn/jetty-alpn-api/* jetty-alpn/jetty-alpn-conscrypt-server/* jetty-alpn/jetty-alpn-java-server/src/test/resources/* jetty-alpn/jetty-alpn-openjdk8-server/src/test/resources/* jetty-annotations/* jetty-annotations/src/test/resources/* jetty-annotations/src/test/resources/jdk10/* jetty-annotations/src/test/resources/jdk9/* jetty-ant/src/test/* jetty-client/* jetty-deploy/* jetty-fcgi/* jetty-http/* jetty-http/src/test/resources/multipart/* jetty-http2/http2-alpn-tests/* jetty-http2/http2-http-client-transport/* jetty-io/* jetty-jmh/* jetty-jmh/src/main/resources/multipart/* jetty-jmx/* jetty-osgi/test-jetty-osgi/* jetty-osgi/test-jetty-osgi/src/test/config/* jetty-proxy/* jetty-proxy/src/test/resources/client_auth/* jetty-server/* jetty-server/src/main/config/* jetty-server/src/test/config/* jetty-servlet/* jetty-servlets/src/test/resources/* jetty-start/src/test/resources/* jetty-util/src/test/* jetty-util/src/test/resources/TestData/* jetty-webapp/src/test/resources/* jetty-webapp/src/test/resources/wars/* jetty-webapp/src/test/webapp/* jetty-websocket/websocket-common/* jetty-websocket/websocket-server/* tests/test-distribution/src/test/resources/badapp/* tests/test-http-client-transport/* tests/test-integration/* tests/test-sessions/test-mongodb-sessions/* tests/test-webapps/test-http2-webapp/* tests/test-webapps/test-jaas-webapp/* tests/test-webapps/test-jetty-webapp/* tests/test-webapps/test-jndi-webapp/src/main/webapp/* tests/test-webapps/test-servlet-spec/test-spec-webapp/src/main/webapp/*
Copyright: 2006-2022, Mort Bay Consulting Pty. Ltd.
License: Apache-2.0 or EPL-1.0

Files: NOTICE.txt
Copyright: has been assignedthe authors (eg. employer).
License: Apache-2.0 and/or EPL-1.0 and/or MIT~UnixCrypt

Files: aggregates/*
Copyright: the combination of files containing Original Software with files containing Modifications, in each case including portions thereof.
License: (CDDL-1.0 and/or GPL-2+) with Classpath-2.0 exception

Files: apache-jsp/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: apache-jstl/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: debian/*
Copyright: 2003, Philipp Meier <meier@meisterbohne.de>
            2009, Ludovic Claude <ludovic.claude@laposte.net>
            2009, David Yu <david.yu.ftw@gmail.com>
            2012-2022, Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
License: Apache-2.0

Files: debian/patches/02-import-alpn-api.patch
Copyright: 2008-2015, Mort Bay Consulting Pty. Ltd.</Bundle-Copyright>
License: EPL-1.0

Files: debian/patches/CVE-2023-36478.patch
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: examples/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: header-template-doc.txt
 header-template-java.txt
Copyright: no-info-found
License: EPL-1.0

Files: jetty-alpn/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-alpn/jetty-alpn-api/src/*
Copyright: 1995-2015, Mort Bay Consulting Pty. Ltd.
License: EPL-1.0

Files: jetty-alpn/jetty-alpn-conscrypt-server/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-alpn/jetty-alpn-conscrypt-server/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-annotations/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-annotations/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-ant/*
Copyright: 1995-2012, Sabre Holdings.
License: EPL-1.0

Files: jetty-ant/src/main/java/org/eclipse/jetty/ant/AntWebInfConfiguration.java
 jetty-ant/src/main/java/org/eclipse/jetty/ant/JettyStopTask.java
 jetty-ant/src/main/java/org/eclipse/jetty/ant/package-info.java
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-ant/src/main/java/org/eclipse/jetty/ant/types/Attribute.java
 jetty-ant/src/main/java/org/eclipse/jetty/ant/types/Attributes.java
 jetty-ant/src/main/java/org/eclipse/jetty/ant/types/Connector.java
 jetty-ant/src/main/java/org/eclipse/jetty/ant/types/package-info.java
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-ant/src/main/java/org/eclipse/jetty/ant/utils/package-info.java
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-ant/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-cdi/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-client/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-client/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-continuation/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-deploy/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-deploy/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-distribution/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-fcgi/fcgi-client/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-fcgi/fcgi-server/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-fcgi/fcgi-server/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-gcloud/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-gcloud/jetty-gcloud-session-manager/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-gcloud/jetty-gcloud-session-manager/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-hazelcast/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-hazelcast/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-hazelcast/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-home/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-http-spi/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-http/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-http/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-http2/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-http2/http2-alpn-tests/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-http2/http2-http-client-transport/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-http2/http2-http-client-transport/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-infinispan/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-infinispan/infinispan-common/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-infinispan/infinispan-common/src/main/java/org/eclipse/jetty/session/infinispan/InfinispanSerializationContextInitializer.java
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-2.0

Files: jetty-infinispan/infinispan-embedded-query/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-infinispan/infinispan-remote-query/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-infinispan/infinispan-remote-query/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-io/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-io/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-jaas/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-jaspi/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-jmh/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-jmx/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-jmx/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-jndi/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-jspc-maven-plugin/*
Copyright: 1995-2020, Mort Bay Consulting Pty Ltd and others. ->
License: EPL-1.0

Files: jetty-jspc-maven-plugin/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-maven-plugin/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-maven-plugin/src/it/jetty-deploy-war-mojo-it/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-maven-plugin-provided-module-dep/postbuild.groovy
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-run-mojo-it/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-run-mojo-it/jetty-simple-base/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-maven-plugin/src/it/jetty-run-mojo-jsp/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-run-mojo-jsp/src/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-maven-plugin/src/it/jetty-run-mojo-multi-module-single-war-it/postbuild.groovy
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-run-war-exploded-mojo-it/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-run-war-exploded-mojo-it/jetty-simple-base/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-maven-plugin/src/it/jetty-run-war-mojo-it/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-run-war-mojo-it/jetty-simple-base/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-maven-plugin/src/it/jetty-start-mojo-it/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-maven-plugin/src/it/jetty-start-mojo-it/jetty-simple-base/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-maven-plugin/src/it/run-mojo-gwt-it/postbuild.groovy
Copyright: no-info-found
License: Apache-2.0

Files: jetty-memcached/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-memcached/jetty-memcached-sessions/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-memcached/jetty-memcached-sessions/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-nosql/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-nosql/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-openid/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-osgi/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-osgi/test-jetty-osgi/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-plus/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-proxy/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-proxy/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-quickstart/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-rewrite/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-runner/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-security/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-server/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-server/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-servlet/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-servlet/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-servlets/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-spring/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-start/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-unixsocket/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-unixsocket/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-unixsocket/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-util-ajax/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-util/*
Copyright: no-info-found
License: Apache-2.0

Files: jetty-util/src/main/config/modules/logback-impl.mod
Copyright: 1999-2012, QOS.ch.
License: LGPL-2.1

Files: jetty-util/src/main/config/modules/slf4j-api.mod
Copyright: 2004-2013, QOS.ch
License: Expat

Files: jetty-util/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-util/src/main/java/org/eclipse/jetty/util/Utf8Appendable.java
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0 and/or Expat

Files: jetty-util/src/main/java/org/eclipse/jetty/util/security/UnixCrypt.java
Copyright: 1996, Aki Yoshida.
License: MIT~UnixCrypt

Files: jetty-util/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-webapp/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-websocket/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-websocket/websocket-common/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-websocket/websocket-common/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-websocket/websocket-server/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-websocket/websocket-server/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-xml/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/test-distribution/src/test/resources/*
Copyright: 2008-2017, Hazelcast, Inc.
License: Apache-2.0

Files: tests/test-http-client-transport/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/test-integration/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/test-sessions/test-mongodb-sessions/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/test-webapps/test-http2-webapp/src/main/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/test-webapps/test-http2-webapp/src/test/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/test-webapps/test-jetty-webapp/src/main/java/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: tests/test-webapps/test-jetty-webapp/src/test/*
Copyright: 1995-2022, Mort Bay Consulting Pty Ltd and others.
License: EPL-1.0

Files: jetty-annotations/src/test/jar/test-sci-with-ordering.jar jetty-annotations/src/test/jar/test-sci.jar jetty-annotations/src/test/resources/jdk9/slf4j-api-1.8.0-alpha2.jar jetty-client/src/test/resources/keystore.p12 jetty-client/src/test/resources/keystore_sni_non_domain.p12 jetty-deploy/src/test/resources/webapps/foo-webapp-1.war jetty-deploy/src/test/resources/webapps/foo-webapp-2.war jetty-deploy/src/test/resources/webapps/foo-webapp-3.war jetty-deploy/src/test/resources/webapps/logcommon.war jetty-fcgi/fcgi-server/src/test/resources/truststore.jks jetty-http2/http2-alpn-tests/src/test/resources/truststore.jks jetty-osgi/test-jetty-osgi/src/main/resources/truststore.jks jetty-proxy/src/test/resources/client_auth/proxy_keystore.p12 jetty-proxy/src/test/resources/client_auth/server_keystore.p12 jetty-proxy/src/test/resources/client_server_keystore.p12 jetty-proxy/src/test/resources/proxy_keystore.p12 jetty-proxy/src/test/resources/server_keystore.p12 jetty-server/src/test/resources/keystore jetty-server/src/test/resources/keystore_sni.p12 jetty-server/src/test/resources/keystore_sni_key_types.p12 jetty-server/src/test/resources/keystore_sni_nowild.p12 jetty-server/src/test/resources/reload_keystore_2.jks jetty-servlet/src/test/resources/jar-resource-odd.jar jetty-servlet/src/test/resources/truststore.jks jetty-servlets/src/test/resources/jetty_logo.gif jetty-servlets/src/test/resources/jetty_logo.tif jetty-servlets/src/test/resources/jetty_logo.tiff jetty-servlets/src/test/resources/jetty_logo.xcf jetty-servlets/src/test/resources/test.svgz jetty-servlets/src/test/resources/test_quotes.bz2 jetty-servlets/src/test/resources/test_quotes.rar jetty-util/src/test/resources/TestData/test.zip jetty-util/src/test/resources/example.jar jetty-util/src/test/resources/jar-file-resource.jar jetty-util/src/test/resources/keystore jetty-util/src/test/resources/keystore.jce jetty-util/src/test/resources/keystore.p12 jetty-webapp/src/test/webapp/WEB-INF/lib/omega.jar tests/test-http-client-transport/src/test/resources/truststore.jks tests/test-integration/src/test/resources/badKeystore tests/test-integration/src/test/resources/newKeystore tests/test-integration/src/test/resources/oldKeystore tests/test-webapps/test-jaas-webapp/src/main/webapp/images/small_powered_by.gif tests/test-webapps/test-jetty-webapp/src/main/webapp/jetty_banner.gif tests/test-webapps/test-jetty-webapp/src/main/webapp/small_powered_by.gif tests/test-webapps/test-jndi-webapp/src/main/webapp/images/small_powered_by.gif tests/test-webapps/test-servlet-spec/test-spec-webapp/src/main/webapp/images/small_powered_by.gif
Copyright: 2006-2022, Mort Bay Consulting Pty. Ltd.
License: Apache-2.0 or EPL-1.0
